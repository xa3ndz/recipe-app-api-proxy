FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer=xa3ndz@gmail.com

COPY /default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uswgi_params /etc/nginx/uswgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf.tpl
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY /entrypoints.sh /entrypoints.sh
RUN chmod +x /entrypoints.sh

USER nginx

CMD ["/entrypoint.sh"]
