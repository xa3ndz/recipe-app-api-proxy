#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.etl > /etc/nginx/conf.default/conf

nginx -g 'daemon off';
